<?php

class Hewan{
    public $Nama;
    public $Darah;
    public $JumlahKaki;
    public $Keahlian;

    public function __construct($Nama, $Darah, $JumlahKaki, $Keahlian, $AttackPower, $DefencePower){
        $this->Nama = $Nama;
        $this->Darah = $Darah;
        $this->JumlahKaki = $JumlahKaki;
        $this->Keahlian = $Keahlian;
        $this->AttackPower = $AttackPower;
        $this->DefencePower = $DefencePower;
    }

    public function atraksi(){
        return $this->Nama . ' sedang ' . $this->Keahlian;
    }
}

class Elang extends Hewan{
    public function getInfoHewan(){
        echo
        'Jenis Hewan = ' . $this->Nama
          . '<br>' .
        'Darah = ' . $this->Darah
          . '<br>' .
        'Jumlah Kaki = ' . $this->JumlahKaki
          . '<br>' .
        'Keahlian = ' . $this->Keahlian
          . '<br>' .
        'Attack Power = ' . $this->AttackPower
          . '<br>' .
        'Defence Power = ' . $this->DefencePower
          . '<br>';
    }
}
class Harimau extends Hewan{
    public function getInfoHewan(){
        echo
        'Jenis Hewan = ' . $this->Nama
          . '<br>' .
        'Darah = ' . $this->Darah
          . '<br>' .
        'Jumlah Kaki = ' . $this->JumlahKaki
          . '<br>' .
        'Keahlian = ' . $this->Keahlian
          . '<br>' .
        'Attack Power = ' . $this->AttackPower
          . '<br>' .
        'Defence Power = ' . $this->DefencePower
          . '<br>';
    }
}


class Fight{
    public $AttackPower;
    public $DefencePower;

    public function serang($Nama1, $Nama2){
        echo '<br>';
        return $Nama1 . ' sedang menyerang ' . $Nama2;
    }

    public function diserang($Diserang, $Darah, $AttackPower, $DefectPower){
        echo '<br>';
        echo $Diserang . ' sedang diserang';
        echo '<br>';
        $Darah = $Darah - $AttackPower / $DefectPower;
        return 'Darah harimau_2 = ' . $Darah;
    }
}

$Elang = new Elang('Elang', 50, 2, "terbang tinggi", 10, 5);
$Harimau = new Harimau('Harimau', 50, 4, "lari cepat", 7, 8);
$Fight = new Fight();

echo '<br>';
echo $Elang->atraksi();
echo '<br>';
echo $Harimau->atraksi();
echo $Fight->serang($Elang->Nama, $Harimau->Nama);
echo $Fight->diserang($Harimau->Nama, $Harimau->Darah, $Elang->AttackPower, $Harimau->DefencePower);

echo '<br>';
$Elang->getInfoHewan();
echo '<br>';
$Harimau->getInfoHewan();

?>
